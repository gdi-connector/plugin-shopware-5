<?php
/**
 * Class Shopware_Controllers_Api_Property
 */
class Shopware_Controllers_Api_Property 
	extends Shopware_Controllers_Api_Rest
{
	/**
	 * @var Shopware\Components\Api\Resource\Property
	 */
	protected $resource;

	public function init()
	{
		$this->resource = \Shopware\Components\Api\Manager::getResource('Property');
	}

	/**
	 * GET Request on /api/Property
	 */
	public function indexAction()
	{
		$result = $this->resource->getList();

		$this->View()->assign(['success' => true, 'data' => $result]);
	}


	/**
	 * Get one Banner
	 *
	 * GET /api/Banner/{id}
	 */
	public function getAction()
	{
		$id = $this->Request()->getParam('id');
		/** @var \Shopware\Models\Banner\Banner $banner */
		$propertyList = $this->resource->getOne($id);

		$this->View()->assign(['success' => true, 'property' => $id, 'data' => $propertyList]);
	}

}
