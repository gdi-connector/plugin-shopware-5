<?php
/**
 * Class Shopware_Controllers_Api_Orderaddress
 */
class Shopware_Controllers_Api_Orderaddress 
	extends Shopware_Controllers_Api_Rest
{
	/**
	 * @var Shopware\Components\Api\Resource\Orderaddress
	 */
	protected $resource;

	public function init()
	{
		$this->resource = \Shopware\Components\Api\Manager::getResource('Orderaddress');
	}

	/**
	 * GET Request on /api/Orderaddress
	 */
	public function indexAction()
	{
		$orderId = $this->Request()->getParam('orderId', '');
		$customerId = $this->Request()->getParam('customerId', '');		
		$result = $this->resource->getList($orderId, $customerId);

		$this->View()->assign(['success' => true, 'data' => $result]);
	}


	/**
	 *
	 * GET /api/Orderaddress/{id}
	 */
	public function getAction()
	{
		$id = $this->Request()->getParam('id');
		$propertyList = $this->resource->getOne($id);

		$this->View()->assign(['success' => true, 'property' => $id, 'data' => $propertyList]);
	}

}
