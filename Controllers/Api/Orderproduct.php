<?php
/**
 * Class Shopware_Controllers_Api_Orderproduct
 */
class Shopware_Controllers_Api_Orderproduct 
	extends Shopware_Controllers_Api_Rest
{
	/**
	 * @var Shopware\Components\Api\Resource\Orderproduct
	 */
	protected $resource;

	public function init()
	{
		$this->resource = \Shopware\Components\Api\Manager::getResource('Orderproduct');
	}

	/**
	 * GET Request on /api/Orderproduct
	 */
	public function indexAction()
	{
		$orderId = $this->Request()->getParam('orderId', '');
		$result = $this->resource->getList($orderId, $customerId);

		$this->View()->assign(['success' => true, 'data' => $result]);
	}

}
