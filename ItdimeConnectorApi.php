<?php
namespace ItdimeConnectorApi;
use Shopware\Components\Plugin;

class ItdimeConnectorApi 
	extends Plugin
{
	/**
	 * @inheritdoc
	 */
	public static function getSubscribedEvents()
	{
		return [
			'Enlight_Controller_Dispatcher_ControllerPath_Api_Property' => 'onGetPropertyApiController',
			'Enlight_Controller_Dispatcher_ControllerPath_Api_Orderaddress' => 'onGetOrderaddressApiController',
			'Enlight_Controller_Dispatcher_ControllerPath_Api_Orderproduct' => 'onGetOrderproductApiController',
			'Enlight_Controller_Front_StartDispatch' => 'onEnlightControllerFrontStartDispatch'
		];
	}

	/**
	 * @return string
	 */
	public function OffonGetPropertyApiController()
	{
		return $this->getPath() . '/Controllers/Api/Property.php';
	}
	
	public function onGetOrderaddressApiController()
	{
		return $this->getPath() . '/Controllers/Api/Orderaddress.php';
	}

	public function onGetOrderproductApiController()
	{
		return $this->getPath() . '/Controllers/Api/Orderproduct.php';
	}

	/**
	 *
	 */
	public function onEnlightControllerFrontStartDispatch()
	{
		$this->container->get('loader')->registerNamespace('Shopware\Components', $this->getPath() . '/Components/');
	}
}
