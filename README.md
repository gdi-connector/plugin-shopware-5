Das Plugin dient als Erweiterung zur on-board REST API von Shopware v5.

Es werden zuusätzlich benötigte Informationen, i.d.R. Meta Daten, vom shop abgefragt.

### Version ###

* Version: 1.0.0
* getestet in Shopware: 5.x

### Plugin in Magento implementieren ###

* Installation im Verzeichnis [MAGENTO_ROOT]\..\ItdimeConnectorApi\