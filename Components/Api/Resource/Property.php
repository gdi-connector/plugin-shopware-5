<?php
namespace Shopware\Components\Api\Resource;

use Shopware\Components\Api\Exception as ApiException;
//use Shopware\Models\Banner\Banner as BannerModel;

/**
 * Class Banner
 *
 * @package Shopware\Components\Api\Resource
 */
class Property 
	extends Resource
{
	/**
	 * @return \Shopware\Models\Banner\Repository
	 */
	public function getRepository($modelName)
	{
		$namespaces = [
			'tax' => ['Tax', 'Tax'],
			'payment' => ['Payment', 'Payment'],
			'shipping' => ['Dispatch', 'Dispatch'],
			'manufacture' => ['Article', 'Supplier'],
			'unit' => ['Article', 'Unit'],
			'orderStatus' => ['Order', 'Status'],
			'paymentStatus' => ['Order', 'Status'],
			'customerStatus' => ['Customer', 'Group']
		];
		
		return $this->getManager()->getRepository('Shopware\\Models\\'.$namespaces[$modelName][0].'\\'.$namespaces[$modelName][0]);
	}


	/**
	 * @return array List of available properties
	 */
	public function getList()
	{
		$posibleProperties = [
			'tax',
			'payment',
			'shipping'
		];

		return ['data' => $posibleProperties, 'total' => count($posibleProperties) ];
	}

	/**
	 * Get List of asked Property
	 *
	 * @param $id
	 * @return mixed
	 * @throws ApiException\NotFoundException
	 * @throws ApiException\ParameterMissingException
	 */
	public function getOne($id)
	{
		if($id == 'orderStatus')
			$builder = $this->getRepository($id)->getOrderStatusQueryBuilder();

		else if($id == 'paymentStatus')
			$builder = $this->getRepository($id)->getPaymentStatusQueryBuilder();
		
		else if($id == 'customerStatus')
			$builder = $this->getRepository($id)->getCustomerGroupsQueryBuilder();

		else if($id == 'manufacture')
			$builder = $this->getRepository($id)->getSupplierListQueryBuilder(null, []);
		
		else if($id == 'unit')
			$builder = $this->getRepository($id)->getUnitsQueryBuilder(null, []);
		
		else if($id == 'articlesAttribute') {
        $builder = Shopware()->Container()->get('models')->createQueryBuilder();
        $builder
						->select(['configuration'])
            ->from('Shopware\Models\Attribute\Configuration', 'configuration')
            ->where('configuration.tableName = ?1')
            ->setParameter(1, 's_articles_attributes');
		}
		
		else if(in_array($id, $this->getList()['data'])) 
			$builder = $this->getRepository($id)->createQueryBuilder($id);
		
		else
			return null;
		
		$query = $builder->getQuery();
		$query->setHydrationMode($this->resultMode);

		$paginator = $this->getManager()->createPaginator($query);

		//returns the total count of the query
		$totalResult = $paginator->count();

		//returns the Banner data
		$list = $paginator->getIterator()->getArrayCopy();

		return $list;
	}

}
